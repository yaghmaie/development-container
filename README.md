# Development Container

This repository is a basic example to demonstrate an efficient while reliable way to use a development container in Gitlab CI.

Please read the post at [DEV Community](https://dev.to/yaghmaie/development-container-gitlab-ci-build-better-and-faster-16ob).
